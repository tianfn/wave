package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.InvocationTargetException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.SwingConstants;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;

import ChartDirector.Chart;
import ChartDirector.ChartViewer;
import ChartDirector.DrawArea;
import ChartDirector.Layer;
import ChartDirector.LineLayer;
import ChartDirector.PlotArea;
import ChartDirector.RanSeries;
import ChartDirector.TTFText;
import ChartDirector.TrackCursorAdapter;
import ChartDirector.ViewPortAdapter;
import ChartDirector.ViewPortChangedEvent;
import ChartDirector.XYChart;


@SuppressWarnings("serial")
public class WaveFile extends Frame {
    private Frame frame;
    private ChartViewer chartViewer;
    private String fileName;
    private int length = 0;
    private Date[] timeStamps;
    private double[] dataSeriesA;
    private double[] dataSeriesB;
    private double[] dataSeriesC;
    private double[] dataSeriesD;
    private double data[][];
    private boolean hasFinishedInitialization;
    private JScrollBar hScrollBar1;
    private boolean choose_a = false;
    private boolean choose_b = false;
    private boolean choose_c = false;
    private boolean choose_d = false;
    private double up_limit = 400, down_limit = -400;
    private boolean isScrolled = false;

    public void setScrolled(boolean isScrolled) {
        this.isScrolled = isScrolled;
    }

    public void setUp_limit(double up_limit) {
        this.up_limit = up_limit;
    }

    public void setDown_limit(double down_limit) {
        this.down_limit = down_limit;
    }

    public int getLength() {
        return length;
    }

    public void setFileName(final String fileName) {
        this.fileName = fileName;

        ProgressMonitorDialog progress = new ProgressMonitorDialog(null);
        try {
            progress.run(true, false, new IRunnableWithProgress() {
                public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
                    monitor.beginTask("正在打开...", IProgressMonitor.UNKNOWN);
                    monitor.setTaskName("正在打开...");
                    length = getLine(fileName);
                    data = getNum(fileName, hScrollBar1.getValue() + 1, 30000);
                    monitor.done();
                }
            });
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        dataSeriesA = data[0];
        dataSeriesB = data[1];
        dataSeriesC = data[2];
        dataSeriesD = data[3];
        hScrollBar1.setMaximum(length);
    }

    public boolean isChoose_a() {
        return choose_a;
    }

    public void setChoose_a(boolean choose_a) {
        this.choose_a = choose_a;
    }

    public boolean isChoose_b() {
        return choose_b;
    }

    public void setChoose_b(boolean choose_b) {
        this.choose_b = choose_b;
    }

    public boolean isChoose_c() {
        return choose_c;
    }

    public void setChoose_c(boolean choose_c) {
        this.choose_c = choose_c;
    }

    public boolean isChoose_d() {
        return choose_d;
    }

    public void setChoose_d(boolean choose_d) {
        this.choose_d = choose_d;
    }

    public ChartViewer getChartViewer() {
        return chartViewer;
    }

    public WaveFile(String fileName, Frame frame) {
        super();
        this.frame = frame;
        this.fileName = fileName;
        creatChartView();
    }

    private void creatChartView() {
        length = getLine(fileName);

        chartViewer = new ChartViewer();
        chartViewer.setBackground(new java.awt.Color(255, 255, 255));
        chartViewer.setOpaque(true);
        chartViewer.setPreferredSize(new Dimension(frame.getWidth(), frame.getHeight()));
        chartViewer.setHorizontalAlignment(SwingConstants.CENTER);
        chartViewer.setHotSpotCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        chartViewer.addViewPortListener(new ViewPortAdapter() {
            public void viewPortChanged(ViewPortChangedEvent e) {
                chartViewer1_ViewPortChanged(e);
            }
        });
        chartViewer.addTrackCursorListener(new TrackCursorAdapter() {
            public void mouseMovedPlotArea(MouseEvent e) {
                chartViewer1_MouseMovedPlotArea(e);
            }
        });

        chartViewer.addComponentListener(new ComponentListener() {

            public void componentShown(ComponentEvent arg0) {
                // TODO 自动生成的方法存根

            }

            public void componentResized(ComponentEvent arg0) {
                // TODO 自动生成的方法存根
                drawChart(chartViewer, frame.getWidth(), frame.getHeight());

            }

            public void componentMoved(ComponentEvent arg0) {
                // TODO 自动生成的方法存根

            }

            public void componentHidden(ComponentEvent arg0) {
                // TODO 自动生成的方法存根

            }
        });

        chartViewer.addMouseWheelListener(new MouseWheelListener() {

            @Override
            public void mouseWheelMoved(MouseWheelEvent arg0) {
                // TODO 自动生成的方法存根
                isScrolled = true;
                chartViewer1_MouseWheel(arg0);
            }
        });

        // Horizontal Scroll bar
        hScrollBar1 = new JScrollBar(JScrollBar.HORIZONTAL, 0, length, 0, length);
        hScrollBar1.addAdjustmentListener(new AdjustmentListener() {
            public void adjustmentValueChanged(AdjustmentEvent e) {
                hScrollBar1_ValueChanged();
            }
        });

        // Put the ChartViewer and the scroll bars in the right panel
        JPanel rightPanel = new JPanel(new BorderLayout());
        rightPanel.add(chartViewer, java.awt.BorderLayout.CENTER);
        rightPanel.add(hScrollBar1, java.awt.BorderLayout.SOUTH);

        // Put the leftPanel and rightPanel on the content pane
        frame.add(rightPanel, java.awt.BorderLayout.CENTER);

        // Layout the window
        pack();
        loadData(1, 30000);

        data = getNum(fileName, hScrollBar1.getValue() + 1, 30000);
        dataSeriesA = data[0];
        dataSeriesB = data[1];
        dataSeriesC = data[2];
        dataSeriesD = data[3];

        initChartViewer(chartViewer);

        // It is safe to handle events now.
        hasFinishedInitialization = true;

        // Trigger a view port update to draw chart.
        chartViewer.updateViewPort(true, true);
        chartViewer.setDoubleBuffered(true);
    }

    private void chartViewer1_MouseWheel(MouseWheelEvent e) {
        // We zoom in or out by 10% depending on the mouse wheel direction.
        double rx = e.getWheelRotation() < 0 ? 0.9 : 1 / 0.9;
        double ry = rx;
        // We do not zoom in beyond the zoom in width or height limit.
        rx = Math.max(rx, chartViewer.getZoomInWidthLimit() / chartViewer.getViewPortWidth());
        ry = Math.max(ry, chartViewer.getZoomInWidthLimit() / chartViewer.getViewPortHeight());
        if ((rx == 1) && (ry == 1)) {
            return;
        }

        XYChart c = (XYChart) chartViewer.getChart();
        chartViewer.setZoomInHeightLimit(0.001);
        chartViewer.setZoomInWidthLimit(0.001);

        //
        // Set the view port position and size so that it is zoom in/out
        // around
        // the mouse by the
        // desired ratio.
        //

        double mouseOffset = (e.getX() - c.getPlotArea().getLeftX()) / (double) c.getPlotArea().getWidth();
        chartViewer.setViewPortLeft(chartViewer.getViewPortLeft() + mouseOffset * (1 - rx) * chartViewer.getViewPortWidth());
        chartViewer.setViewPortWidth(chartViewer.getViewPortWidth() * rx);

        double mouseOffsetY = (e.getY() - c.getPlotArea().getTopY()) / (double) c.getPlotArea().getHeight();
        chartViewer.setViewPortTop(chartViewer.getViewPortTop() + mouseOffsetY * (1 - ry) * chartViewer.getViewPortHeight());
        chartViewer.setViewPortHeight(chartViewer.getViewPortHeight() * ry);

        // Trigger a view port changed event to update the chart
        chartViewer.updateViewPort(true, false);
    }

    public static int getLine(String filename) {
        if (filename == null) {
            return 30000;
        }
        File file = new File(filename);
        long len = file.length();
        MappedByteBuffer buffer = null;
        int t = 0;
        try {
            buffer = new RandomAccessFile(filename, "r").getChannel().map(FileChannel.MapMode.READ_ONLY, 0, len);
            for (int i = 0; i < len; i++) {
                if (buffer.get(i) == ('o') && buffer.get(i + 1) == ('v') && buffer.get(i + 2) == ('e') && buffer.get(i + 3) == ('r')) {
                    break;
                }
                t++;
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (EOFException e) {
            // TODO: handle exception
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return t / 32;
    }

    private double[][] getNum(String filename, int start, int num) {
        double[][] data = new double[4][num];
        if (filename == null) {
            data[0] = new double[] { 0 };
            data[1] = new double[] { 0 };
            data[2] = new double[] { 0 };
            data[3] = new double[] { 0 };
            return data;
        }
        File file = new File(filename);
        long len = file.length();
        int s = 0;
        s = (start - 1) * 32;

        MappedByteBuffer buffer = null;
        try {
            buffer = new RandomAccessFile(filename, "r").getChannel().map(FileChannel.MapMode.READ_ONLY, 0, len);
            for (int i = 0; i < num; i++) {
                if (start + i < length) {
                    data[0][i] = buffer.getDouble(s + 0 + 32 * i);
                    data[1][i] = buffer.getDouble(s + 8 + 32 * i);
                    data[2][i] = buffer.getDouble(s + 16 + 32 * i);
                    data[3][i] = buffer.getDouble(s + 24 + 32 * i);
                }
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return data;
    }

    private void loadData(int start, int num) {
        // In this example, we just use random numbers as data.
        RanSeries r = new RanSeries(127);
        timeStamps = r.getDateSeries(num, new GregorianCalendar(2010, 0, 1).getTime(), 86400);
    }

    private void initChartViewer(ChartViewer viewer) {
        viewer.setFullRange("x", timeStamps[0], timeStamps[timeStamps.length - 1]);

        viewer.setViewPortWidth(0.2);
        viewer.setViewPortLeft(1 - viewer.getViewPortWidth());

        viewer.setZoomInWidthLimit(10.0 / timeStamps.length);

        viewer.setMouseWheelZoomRatio(1.1);
        chartViewer.setMouseUsage(Chart.MouseUsageScrollOnDrag);
    }

    private void chartViewer1_ViewPortChanged(ViewPortChangedEvent e) {
        updateControls(chartViewer);

        // Update the chart if necessary
        if (e.needUpdateChart())
            drawChart(chartViewer, frame.getWidth(), frame.getHeight());
    }

    public void refresh() {
        drawChart(chartViewer, frame.getWidth(), frame.getHeight());
    }

    //
    // Update controls when the view port changed
    //
    private void updateControls(ChartViewer viewer) {
        hScrollBar1.setVisibleAmount(5);
        hScrollBar1.setBlockIncrement(hScrollBar1.getVisibleAmount());
        hScrollBar1.setUnitIncrement((int) Math.ceil(hScrollBar1.getVisibleAmount() * 0.1));
    }

    //
    // Draw the chart.
    //
    private void drawChart(ChartViewer viewer, int width, int height) {
        // Get the start date and end date that are visible on the chart.
        Date viewPortStartDate = Chart.NTime(viewer.getValueAtViewPort("x", viewer.getViewPortLeft()));
        Date viewPortEndDate = Chart.NTime(viewer.getValueAtViewPort("x", viewer.getViewPortLeft() + viewer.getViewPortWidth()));

        int startIndex = (int) Math.floor(Chart.bSearch(timeStamps, viewPortStartDate));
        int endIndex = (int) Math.ceil(Chart.bSearch(timeStamps, viewPortEndDate));
        int noOfPoints = endIndex - startIndex + 1;
        double[] viewPortDataSeriesA = null, viewPortDataSeriesB = null, viewPortDataSeriesC = null, viewPortDataSeriesD = null;
        if (choose_a) {
            viewPortDataSeriesA = (double[]) Chart.arraySlice(dataSeriesA, startIndex, noOfPoints);
        }
        if (choose_b) {
            viewPortDataSeriesB = (double[]) Chart.arraySlice(dataSeriesB, startIndex, noOfPoints);
        }
        if (choose_c) {
            viewPortDataSeriesC = (double[]) Chart.arraySlice(dataSeriesC, startIndex, noOfPoints);
        }
        if (choose_d) {
            viewPortDataSeriesD = (double[]) Chart.arraySlice(dataSeriesD, startIndex, noOfPoints);
        }

        XYChart c = new XYChart(width, height);

        if (!isScrolled) {
            c.yAxis().setLinearScale(down_limit, up_limit, 0, 0);
        }

        c.recycle(chartViewer.getChart());
        c.setPlotArea(30, 30, c.getWidth() - 40, c.getHeight() - 50);
        c.setClipping();
        c.getLegend().setLineStyleKey();
        LineLayer layer = c.addLineLayer2();
        layer.setLineWidth(2);

        layer.setFastLineMode();

        if (choose_a) {
            layer.addDataSet(viewPortDataSeriesA, 0xffcc00, "#1");
        }
        if (choose_b) {
            layer.addDataSet(viewPortDataSeriesB, 0x2fa366, "#2");
        }
        if (choose_c) {
            layer.addDataSet(viewPortDataSeriesC, 0xff0000, "#3");
        }
        if (choose_d) {
            layer.addDataSet(viewPortDataSeriesD, 0x6710ce, "#4");
        }

        if (!viewer.isInMouseMoveEvent()) {
            trackLineLegend(c, (null == viewer.getChart()) ? c.getPlotArea().getRightX() : viewer.getPlotAreaMouseX());
        }

        viewer.setChart(c);
    }

    //
    // Horizontal ScrollBar ValueChanged event handler
    //
    private void hScrollBar1_ValueChanged() {
        if (hasFinishedInitialization && !chartViewer.isInViewPortChangedEvent()) {
            // Get the view port left as according to the scroll bar
            double newViewPortLeft = ((double) (hScrollBar1.getValue() - hScrollBar1.getMinimum()))
                    / (hScrollBar1.getMaximum() - hScrollBar1.getMinimum());
            if (Math.abs(chartViewer.getViewPortLeft() - newViewPortLeft) > 0.00001 * chartViewer.getViewPortWidth()) {
                // Set the view port based on the scroll bar
                chartViewer.setViewPortLeft(newViewPortLeft);
                data = getNum(this.fileName, hScrollBar1.getValue() + 1, 30000);
                dataSeriesA = data[0];
                dataSeriesB = data[1];
                dataSeriesC = data[2];
                dataSeriesD = data[3];
                chartViewer.updateViewPort(true, false);
            }
        }
    }

    //
    // Draw track cursor when mouse is moving over plotarea
    //
    private void chartViewer1_MouseMovedPlotArea(MouseEvent e) {
        ChartViewer viewer = (ChartViewer) e.getSource();
        trackLineLegend((XYChart) viewer.getChart(), viewer.getPlotAreaMouseX());
        viewer.updateDisplay();
    }

    //
    // Draw the track line with legend
    //
    private void trackLineLegend(XYChart c, int mouseX) {
        // Clear the current dynamic layer and get the DrawArea object to draw
        // on it.
        DrawArea d = c.initDynamicLayer();

        // The plot area object
        PlotArea plotArea = c.getPlotArea();

        // Get the data x-value that is nearest to the mouse, and find its pixel
        // coordinate.
        double xValue = c.getNearestXValue(mouseX);
        int xCoor = c.getXCoor(xValue);

        // Draw a vertical track line at the x-position
        d.vline(plotArea.getTopY(), plotArea.getBottomY(), xCoor, d.dashLineColor(0x000000, 0x0101));

        // Container to hold the legend entries
        ArrayList legendEntries = new ArrayList();

        // Iterate through all layers to build the legend array
        for (int i = 0; i < c.getLayerCount(); ++i) {
            Layer layer = c.getLayerByZ(i);

            // The data array index of the x-value
            int xIndex = layer.getXIndexOf(xValue);

            // Iterate through all the data sets in the layer
            for (int j = 0; j < layer.getDataSetCount(); ++j) {
                ChartDirector.DataSet dataSet = layer.getDataSetByZ(j);

                // We are only interested in visible data sets with names
                String dataName = dataSet.getDataName();
                int color = dataSet.getDataColor();
                if ((!(dataName == null || dataName.length() == 0)) && (color != Chart.Transparent)) {
                    // Build the legend entry, consist of the legend icon, name
                    // and data value.
                    double dataValue = dataSet.getValue(xIndex);
                    legendEntries.add("<*block*>" + dataSet.getLegendIcon() + " " + dataName + ": "
                            + ((dataValue == Chart.NoValue) ? "N/A" : c.formatValue(dataValue, "{value|P4}")) + "<*/*>");

                    // Draw a track dot for data points within the plot area
                    int yCoor = c.getYCoor(dataSet.getPosition(xIndex), dataSet.getUseYAxis());
                    if ((yCoor >= plotArea.getTopY()) && (yCoor <= plotArea.getBottomY())) {
                        d.circle(xCoor, yCoor, 4, 4, color, color);
                    }
                }
            }
        }

        // Create the legend by joining the legend entries
        Collections.reverse(legendEntries);
        String legendText = "<*block,maxWidth=" + plotArea.getWidth() + Chart.stringJoin(legendEntries, "        ") + "<*/*>";

        // Display the legend on the top of the plot area
        TTFText t = d.text(legendText, "Arial", 8);
        t.draw(plotArea.getLeftX() + 5, plotArea.getTopY() - 3, 0x000000, Chart.BottomLeft);
    }
}
