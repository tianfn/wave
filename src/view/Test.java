package view;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.FormData;

import java.awt.Frame;

import org.eclipse.swt.awt.SWT_AWT;

import java.awt.Dimension;
import java.awt.Panel;
import java.awt.BorderLayout;

import javax.swing.JRootPane;
import javax.swing.SwingConstants;

import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.wb.swt.SWTResourceManager;

import java.awt.Color;
import java.awt.SystemColor;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.custom.StackLayout;

import ChartDirector.Chart;
import ChartDirector.ChartViewer;
import ChartDirector.LineLayer;
import ChartDirector.ViewPortControl;
import ChartDirector.XYChart;
import swing2swt.layout.FlowLayout;
import swing2swt.layout.BoxLayout;

public class Test {

    protected Shell shell;
    double[] data = { 30, 28, 40, 55, 75, 68, 54, 60, 50, 62, 75, 65, 75, 91, 60, 55, 53, 35, 50, 66, 56, 48, 52, 65, 62 };

    // The labels for the line chart
    String[] labels = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21",
            "22", "23", "24" };
    Frame frame_1;
    Frame frame;

    /**
     * Launch the application.
     * 
     * @param args
     */
    public static void main(String[] args) {
        try {
            Test window = new Test();
            window.open();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Open the window.
     */
    public void open() {
        Display display = Display.getDefault();
        createContents();
        shell.open();
        shell.layout();
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }
        System.exit(0);
    }

    /**
     * Create contents of the window.
     */
    protected void createContents() {
        shell = new Shell();
        shell.setSize(800, 800);
        shell.setText("SWT Application");
        shell.setLayout(new FormLayout());

        Composite composite = new Composite(shell, SWT.NONE);
        composite.setBackground(SWTResourceManager.getColor(102, 205, 170));
        composite.setLayout(new FormLayout());
        FormData fd_composite = new FormData();
        fd_composite.width = 150;
        fd_composite.top = new FormAttachment(0, 0);
        fd_composite.bottom = new FormAttachment(100, 0);
        fd_composite.left = new FormAttachment(0, 0);
        // fd_composite.right = new FormAttachment(10, 50,0);

        composite.setLayoutData(fd_composite);

        Button btnNewButton = new Button(composite, SWT.NONE);
        FormData fd_btnNewButton = new FormData();
        fd_btnNewButton.top = new FormAttachment(0, 10);
        fd_btnNewButton.left = new FormAttachment(0, 10);
        fd_btnNewButton.right = new FormAttachment(100, -10);
        btnNewButton.setLayoutData(fd_btnNewButton);
        btnNewButton.setText("\u5F00\u59CB");

        Button btnNewButton_1 = new Button(composite, SWT.NONE);
        FormData fd_btnNewButton_1 = new FormData();
        fd_btnNewButton_1.right = new FormAttachment(btnNewButton, 0, SWT.RIGHT);
        fd_btnNewButton_1.top = new FormAttachment(btnNewButton, 10);
        fd_btnNewButton_1.left = new FormAttachment(btnNewButton, 0, SWT.LEFT);
        btnNewButton_1.setLayoutData(fd_btnNewButton_1);
        btnNewButton_1.setText("\u6682\u505C");

        Button btnNewButton_2 = new Button(composite, SWT.NONE);
        FormData fd_btnNewButton_2 = new FormData();
        fd_btnNewButton_2.right = new FormAttachment(btnNewButton, 0, SWT.RIGHT);
        fd_btnNewButton_2.top = new FormAttachment(btnNewButton_1, 10);
        fd_btnNewButton_2.left = new FormAttachment(btnNewButton, 0, SWT.LEFT);
        btnNewButton_2.setLayoutData(fd_btnNewButton_2);
        btnNewButton_2.setText("\u505C\u6B62");

        Button btnNewButton_3 = new Button(composite, SWT.NONE);
        FormData fd_btnNewButton_3 = new FormData();
        fd_btnNewButton_3.right = new FormAttachment(btnNewButton, 0, SWT.RIGHT);
        fd_btnNewButton_3.top = new FormAttachment(btnNewButton_2, 10);
        fd_btnNewButton_3.left = new FormAttachment(btnNewButton, 0, SWT.LEFT);
        btnNewButton_3.setLayoutData(fd_btnNewButton_3);
        btnNewButton_3.setText("\u4FDD\u5B58");

        Composite composite_1 = new Composite(shell, SWT.EMBEDDED);
        FormData fd_composite_1 = new FormData();
        fd_composite_1.bottom = new FormAttachment(9, 10, 0);
        fd_composite_1.right = new FormAttachment(100, 0);
        fd_composite_1.top = new FormAttachment(0, 0);
        fd_composite_1.left = new FormAttachment(composite, 2);
        composite_1.setLayoutData(fd_composite_1);

        frame = SWT_AWT.new_Frame(composite_1);
        final ViewPortControl vpControl = new ViewPortControl();
        final ChartViewer chartViewer1 = new ChartViewer();
        chartViewer1.setPreferredSize(new Dimension(frame.getWidth(), frame.getHeight()));
        chartViewer1.addComponentListener(new ComponentListener() {

            public void componentShown(ComponentEvent arg0) {
                // TODO 自动生成的方法存根

            }

            public void componentResized(ComponentEvent arg0) {
                // TODO 自动生成的方法存根
                // Create a XYChart object of size 250 x 250 pixels
                XYChart c = new XYChart(frame.getWidth(), frame.getHeight());

                // Set the plotarea at (30, 20) and of size 200 x 200 pixels
                c.setPlotArea(25, 0, frame.getWidth() - 30, frame.getHeight());
                System.out.println(frame.getWidth() + "f" + frame.getHeight());

                // Add a line chart layer using the given data
                c.addLineLayer(data);

                // Set the labels on the x axis.
                c.xAxis().setLabels(labels);

                // Display 1 out of 3 labels on the x-axis.
                c.xAxis().setLabelStep(3);

                // Output the chart
                chartViewer1.setChart(c);

                // include tool tip for the chart
                chartViewer1.setImageMap(c.getHTMLImageMap("clickable", "", "title='Hour {xLabel}: Traffic {value} GBytes'"));
                drawFullChart(vpControl, chartViewer1);

            }

            public void componentMoved(ComponentEvent arg0) {
                // TODO 自动生成的方法存根

            }

            public void componentHidden(ComponentEvent arg0) {
                // TODO 自动生成的方法存根

            }
        });

        // new WaveFile(null,frame);

        Composite composite_2 = new Composite(shell, SWT.EMBEDDED);
        FormData fd_composite_2 = new FormData();
        fd_composite_2.right = new FormAttachment(composite_1, 0, SWT.RIGHT);
        fd_composite_2.bottom = new FormAttachment(100, 0);
        fd_composite_2.left = new FormAttachment(composite_1, 0, SWT.LEFT);
        fd_composite_2.top = new FormAttachment(composite_1, 2);
        composite_2.setLayoutData(fd_composite_2);

        frame_1 = SWT_AWT.new_Frame(composite_2);

        new LineView(frame, frame_1);

        // vpControl.setPreferredSize(new Dimension(frame_1.getWidth(),
        // frame_1.getHeight()));
        // vpControl.setHorizontalAlignment(SwingConstants.CENTER);
        // vpControl.setViewer(chartViewer1);
        // frame_1.add(vpControl);
        // drawFullChart(vpControl, chartViewer1);
    }

    private void drawFullChart(ViewPortControl vpc, ChartViewer viewer) {
        // Create an XYChart object of size 640 x 60 pixels
        XYChart c = new XYChart(frame_1.getWidth(), frame_1.getHeight());

        // Set the plotarea with the same horizontal position as that in the
        // main chart for alignment.
        c.setPlotArea(0, 0, c.getWidth() - 5, c.getHeight(), 0xc0d8ff, -1, 0x888888, Chart.Transparent, 0xffffff);

        // Set the x axis stem to transparent and the label font to 10pt Arial
        c.xAxis().setColors(Chart.Transparent);
        c.xAxis().setLabelStyle("Arial", 7);

        // Put the x-axis labels inside the plot area by setting a negative
        // label gap. Use
        // setLabelAlignment to put the label at the right side of the tick.
        c.xAxis().setLabelGap(-1);
        c.xAxis().setLabelAlignment(1);

        // Set the y axis stem and labels to transparent (that is, hide the
        // labels)
        c.yAxis().setColors(Chart.Transparent, Chart.Transparent);

        // Add a line layer for the lines with fast line mode enabled
        LineLayer layer = c.addLineLayer();
        layer.setFastLineMode();

        // Now we add the 3 data series to a line layer, using the color red
        // (0xff3333), green
        // (0x008800) and blue (0x3333cc)
        layer.addDataSet(data, -1);

        // The x axis scales should reflect the full range of the view port
        c.xAxis().setDateScale(viewer.getValueAtViewPort("x", 0), viewer.getValueAtViewPort("x", 1));

        // For the automatic x-axis labels, set the minimum spacing to 75
        // pixels.
        c.xAxis().setTickDensity(75);

        // For the auto-scaled y-axis, as we hide the labels, we can disable
        // axis rounding. This can
        // make the axis scale fit the data tighter.
        c.yAxis().setRounding(false, false);

        // Output the chart
        vpc.setChart(c);
    }
}
