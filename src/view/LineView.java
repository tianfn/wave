package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.filechooser.FileFilter;

import ChartDirector.Chart;
import ChartDirector.ChartViewer;
import ChartDirector.DrawArea;
import ChartDirector.Layer;
import ChartDirector.LegendBox;
import ChartDirector.LineLayer;
import ChartDirector.PlotArea;
import ChartDirector.TTFText;
import ChartDirector.TrackCursorAdapter;
import ChartDirector.ViewPortAdapter;
import ChartDirector.ViewPortChangedEvent;
import ChartDirector.ViewPortControl;
import ChartDirector.XYChart;

public class LineView {

    private Frame frame1;
    private Frame frame2;
    private ChartViewer chartViewer1;

    public ChartViewer getChartViewer1() {
        return chartViewer1;
    }

    public void setChartViewer1(ChartViewer chartViewer1) {
        this.chartViewer1 = chartViewer1;
    }

    private final int dataInterval = 10;
    private final int sampleSize = 5000;
    private Date[] timeStamps = new Date[sampleSize];
    private double[] dataSeriesA = new double[sampleSize];
    private double[] dataSeriesB = new double[sampleSize];
    private double[] dataSeriesC = new double[sampleSize];

    // The index of the array position to which new data values are added.
    private int currentIndex = 0;

    // The full range is initialized to 60 seconds of data. It can be extended
    // when more data
    // are available.
    private int initialFullRange = 60;

    // The maximum zoom in is 10 seconds.
    private double zoomInLimit = 1;

    // This is an internal variable used by the real time random number
    // generator so it knows what
    // timestamp should be used for the next data point.
    private Date nextDataTime;

    // This flag is used to suppress event handlers before complete
    // initialization
    private boolean hasFinishedInitialization;
    private Timer dataRateTimer;
    private Timer chartUpdateTimer;
    ViewPortControl vpControl = new ViewPortControl();

    public LineView(Frame frame1,Frame frame2) {
        super();
        this.frame1 = frame1;
        this.frame2 = frame2;
        dosth();

    }

    private void drawFullChart(ViewPortControl vpc, ChartViewer viewer) {
        // Create an XYChart object of size 640 x 60 pixels
        XYChart c = new XYChart(frame2.getWidth(), frame2.getHeight());

        // Set the plotarea with the same horizontal position as that in the
        // main chart for alignment.
        c.setPlotArea(30, 0, c.getWidth() - 5, c.getHeight() - 1, 0xc0d8ff, -1, 0x888888, Chart.Transparent, 0xffffff);

        // Set the x axis stem to transparent and the label font to 10pt Arial
        c.xAxis().setColors(Chart.Transparent);
        c.xAxis().setLabelStyle("Arial", 7);

        // Put the x-axis labels inside the plot area by setting a negative
        // label gap. Use
        // setLabelAlignment to put the label at the right side of the tick.
        c.xAxis().setLabelGap(-1);
        c.xAxis().setLabelAlignment(1);

        // Set the y axis stem and labels to transparent (that is, hide the
        // labels)
        c.yAxis().setColors(Chart.Transparent, Chart.Transparent);

        // Add a line layer for the lines with fast line mode enabled
        LineLayer layer = c.addLineLayer();
        layer.setFastLineMode();

        // Now we add the 3 data series to a line layer, using the color red
        // (0xff3333), green
        // (0x008800) and blue (0x3333cc)
        layer.setXData(timeStamps);
        layer.addDataSet(dataSeriesA, -1);
        layer.addDataSet(dataSeriesB, -1);
        layer.addDataSet(dataSeriesC, -1);

        // The x axis scales should reflect the full range of the view port
        c.xAxis().setDateScale(viewer.getValueAtViewPort("x", 0), viewer.getValueAtViewPort("x", 1));

        // For the automatic x-axis labels, set the minimum spacing to 75
        // pixels.
        c.xAxis().setTickDensity(75);

        // For the auto-scaled y-axis, as we hide the labels, we can disable
        // axis rounding. This can
        // make the axis scale fit the data tighter.
        c.yAxis().setRounding(false, false);

        // Output the chart
        vpc.setChart(c);
    }

    private void dosth() {
        nextDataTime = new Date((new Date().getTime()) / 1000 * 1000);
        chartViewer1 = new ChartViewer();
        chartViewer1.setBackground(new java.awt.Color(255, 255, 255));
        chartViewer1.setOpaque(true);
        chartViewer1.setPreferredSize(new Dimension(frame1.getWidth(), frame1.getHeight()));
        chartViewer1.setHorizontalAlignment(SwingConstants.CENTER);
        chartViewer1.setHotSpotCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

        chartViewer1.addComponentListener(new ComponentListener() {

            @Override
            public void componentShown(ComponentEvent arg0) {
                // TODO 自动生成的方法存根

            }

            @Override
            public void componentResized(ComponentEvent arg0) {
                // TODO 自动生成的方法存根
                drawChart(chartViewer1, frame1.getWidth(), frame1.getHeight());
                System.out.println(frame1.getWidth() + "^^" + frame1.getHeight());
            }

            @Override
            public void componentMoved(ComponentEvent arg0) {
                // TODO 自动生成的方法存根

            }

            @Override
            public void componentHidden(ComponentEvent arg0) {
                // TODO 自动生成的方法存根

            }
        });
        chartViewer1.setZoomOutWidthLimit(0.001);
        chartViewer1.setZoomOutHeightLimit(0.001);
        chartViewer1.addViewPortListener(new ViewPortAdapter() {
            public void viewPortChanged(ViewPortChangedEvent e) {
                chartViewer1_viewPortChanged(e);
            }
        });
        chartViewer1.addTrackCursorListener(new TrackCursorAdapter() {
            public void mouseMovedPlotArea(MouseEvent e) {
                chartViewer1_MouseMovedPlotArea(e);
            }
        });
        chartViewer1.addMouseWheelListener(new MouseWheelListener() {

            @Override
            public void mouseWheelMoved(MouseWheelEvent arg0) {
                // TODO 自动生成的方法存根
                // chartViewer1_MouseWheel(arg0);
            }
        });

        JPanel rightPanel = new JPanel(new BorderLayout());
        rightPanel.add(chartViewer1, java.awt.BorderLayout.CENTER);
        frame1.add(rightPanel, java.awt.BorderLayout.CENTER);
        
        vpControl.setPreferredSize(new Dimension(frame2.getWidth(), 100));
        vpControl.setHorizontalAlignment(SwingConstants.CENTER);
        JPanel rightPanel1 = new JPanel(new BorderLayout());
        rightPanel1.add(vpControl, java.awt.BorderLayout.CENTER);
        frame2.add(rightPanel1, java.awt.BorderLayout.CENTER);

        // Trigger a view port update to draw chart.
        chartViewer1.updateViewPort(true, true);
        // The data generation timer for our random number generator
        dataRateTimer = new javax.swing.Timer(dataInterval, new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                dataRateTimer_Tick();
            }
        });

        // The chart update timer
        chartUpdateTimer = new javax.swing.Timer(50, new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                chartUpdateTimer_Tick();
            }
        });

        // Initialize the ChartViewer
        initChartViewer(chartViewer1);

        // It is safe to handle events now.
        hasFinishedInitialization = true;

        // Start collecting and plotting data
        dataRateTimer.start();
        chartUpdateTimer.start();
        vpControl.setViewer(chartViewer1);
    }

    private void chartViewer1_MouseWheel(MouseWheelEvent e) {
        // We zoom in or out by 10% depending on the mouse wheel direction.
        double rx = e.getWheelRotation() < 0 ? 0.9 : 1 / 0.9;
        double ry = rx;
        // We do not zoom in beyond the zoom in width or height limit.
        rx = Math.max(rx, chartViewer1.getZoomInWidthLimit() / chartViewer1.getViewPortWidth());
        ry = Math.max(ry, chartViewer1.getZoomInWidthLimit() / chartViewer1.getViewPortHeight());
        if ((rx == 1) && (ry == 1)) {
            return;
        }

        XYChart c = (XYChart) chartViewer1.getChart();
        chartViewer1.setZoomOutWidthLimit(0.01);
        chartViewer1.setZoomOutHeightLimit(0.01);

        //
        // Set the view port position and size so that it is zoom in/out
        // around
        // the mouse by the
        // desired ratio.
        //

        double mouseOffset = (e.getX() - c.getPlotArea().getLeftX()) / (double) c.getPlotArea().getWidth();
        chartViewer1.setViewPortLeft(chartViewer1.getViewPortLeft() + mouseOffset * (1 - rx) * chartViewer1.getViewPortWidth());
        chartViewer1.setViewPortWidth(chartViewer1.getViewPortWidth() * rx);

        double mouseOffsetY = (e.getY() - c.getPlotArea().getTopY()) / (double) c.getPlotArea().getHeight();
        chartViewer1.setViewPortTop(chartViewer1.getViewPortTop() + mouseOffsetY * (1 - ry) * chartViewer1.getViewPortHeight());
        chartViewer1.setViewPortHeight(chartViewer1.getViewPortHeight() * ry);

        // Trigger a view port changed event to update the chart
        chartViewer1.updateViewPort(true, false);
    }

    //
    // A utility to load an image icon from the Java class path
    //
    private ImageIcon loadImageIcon(String path) {
        try {
            return new ImageIcon(getClass().getClassLoader().getResource(path));
        } catch (Exception e) {
            return null;
        }
    }

    //
    // Initialize the WinChartViewer
    //
    private void initChartViewer(ChartViewer viewer) {
        // Enable mouse wheel zooming by setting the zoom ratio to 1.1 per wheel
        // event
        viewer.setMouseWheelZoomRatio(1.1);
        chartViewer1.setMouseUsage(Chart.MouseUsageScrollOnDrag);

        // Initially set the mouse usage to "Pointer" mode (Drag to Scroll mode)
    }

    //
    // The data update routine. In this demo, it is invoked every 250ms to get
    // new data.
    //
    private void dataRateTimer_Tick() {
        Date now = new Date();
        do {
            //
            // In this demo, we use some formulas to generate new values. In
            // real applications,
            // it may be replaced by some data acquisition code.
            //
            double p = nextDataTime.getTime() / 1000.0 * 4;
            double dataA = 20 + Math.cos(p * 2.2) * 10 + 1 / (Math.cos(p) * Math.cos(p) + 0.01);
            double dataB = 150 + 100 * Math.sin(p / 27.7) * Math.sin(p / 10.1);
            double dataC = 150 + 100 * Math.cos(p / 6.7) * Math.cos(p / 11.9);

            // In this demo, if the data arrays are full, the oldest 5% of data
            // are discarded.
            if (currentIndex >= timeStamps.length) {
                currentIndex = sampleSize * 95 / 100 - 1;
                System.arraycopy(timeStamps, sampleSize - currentIndex, timeStamps, 0, currentIndex);
                System.arraycopy(dataSeriesA, sampleSize - currentIndex, dataSeriesA, 0, currentIndex);
                System.arraycopy(dataSeriesB, sampleSize - currentIndex, dataSeriesB, 0, currentIndex);
                System.arraycopy(dataSeriesC, sampleSize - currentIndex, dataSeriesC, 0, currentIndex);
            }

            // Store the new values in the current index position, and increment
            // the index.
            timeStamps[currentIndex] = nextDataTime;
            dataSeriesA[currentIndex] = dataA;
            dataSeriesB[currentIndex] = dataB;
            dataSeriesC[currentIndex] = dataC;
            ++currentIndex;

            // Update nextDataTime
            nextDataTime = new Date(nextDataTime.getTime() + dataInterval);
            // System.out.println(nextDataTime.getTime());
        } while (nextDataTime.before(now));

    }

    //
    // The chartUpdateTimer Tick event - this updates the chart periodicially by
    // raising
    // viewPortChanged events.
    //
    private void chartUpdateTimer_Tick() {
        drawFullChart(vpControl, chartViewer1);
        ChartViewer viewer = chartViewer1;

        if (currentIndex >= 0) {
            //
            // As we added more data, we may need to update the full range.
            //

            Date startDate = timeStamps[0];
            Date endDate = timeStamps[currentIndex - 1];

            // Use the initialFullRange if this is sufficient.
            double duration = endDate.getTime() - startDate.getTime();
            if (duration < initialFullRange * 1000)
                endDate = new Date(startDate.getTime() + initialFullRange * 1000);

            // Update the full range to reflect the actual duration of the data.
            // In this case,
            // if the view port is viewing the latest data, we will scroll the
            // view port as new
            // data are added. If the view port is viewing historical data, we
            // would keep the
            // axis scale unchanged to keep the chart stable.
            int updateType = Chart.ScrollWithMax;
            if (viewer.getViewPortLeft() + viewer.getViewPortWidth() < 0.999)
                updateType = Chart.KeepVisibleRange;
            boolean axisScaleHasChanged = viewer.updateFullRangeH("x", startDate, endDate, updateType);

            // Set the zoom in limit as a ratio to the full range
            viewer.setZoomInWidthLimit(zoomInLimit / (viewer.getValueAtViewPort("x", 1) - viewer.getValueAtViewPort("x", 0)));

            // Trigger the viewPortChanged event to update the display if the
            // axis scale has
            // changed or if new data are added to the existing axis scale.
            if (axisScaleHasChanged || (duration < initialFullRange * 1000))
                viewer.updateViewPort(true, false);
            // Draw the full thumbnail chart for the ViewPortControl

        }
    }

    //
    // The viewPortChanged event handler. In this example, it just updates the
    // chart. If you
    // have other controls to update, you may also put the update code here.
    //
    private void chartViewer1_viewPortChanged(ViewPortChangedEvent e) {
        // In addition to updating the chart, we may also need to update other
        // controls that
        // changes based on the view port.
        updateControls(chartViewer1);

        // Update the chart if necessary
        if (e.needUpdateChart())
            drawChart(chartViewer1, frame1.getWidth(), frame1.getHeight());
    }

    //
    // Update other controls when the view port changed
    //
    private void updateControls(ChartViewer viewer) {
        // Update the scroll bar to reflect the view port position and width of
        // the view port.
    }

    //
    // Draw the chart and display it in the given viewer.
    //
    private void drawChart(ChartViewer viewer, int width, int height) {
        // Get the start date and end date that are visible on the chart.
        Date viewPortStartDate = Chart.NTime(viewer.getValueAtViewPort("x", viewer.getViewPortLeft()));
        Date viewPortEndDate = Chart.NTime(viewer.getValueAtViewPort("x", viewer.getViewPortLeft() + viewer.getViewPortWidth()));

        // Extract the part of the data arrays that are visible.
        Date[] viewPortTimeStamps = null;
        double[] viewPortDataSeriesA = null;
        double[] viewPortDataSeriesB = null;
        double[] viewPortDataSeriesC = null;

        if (currentIndex > 0) {
            // Get the array indexes that corresponds to the visible start and
            // end dates
            int startIndex = (int) Math.floor(Chart.bSearch2(timeStamps, 0, currentIndex, viewPortStartDate));
            int endIndex = (int) Math.ceil(Chart.bSearch2(timeStamps, 0, currentIndex, viewPortEndDate));
            int noOfPoints = endIndex - startIndex + 1;

            // Extract the visible data
            viewPortTimeStamps = (Date[]) Chart.arraySlice(timeStamps, startIndex, noOfPoints);
            viewPortDataSeriesA = (double[]) Chart.arraySlice(dataSeriesA, startIndex, noOfPoints);
            viewPortDataSeriesB = (double[]) Chart.arraySlice(dataSeriesB, startIndex, noOfPoints);
            viewPortDataSeriesC = (double[]) Chart.arraySlice(dataSeriesC, startIndex, noOfPoints);
        }

        //
        // At this stage, we have extracted the visible data. We can use those
        // data to plot the chart.
        //

        // ================================================================================
        // Configure overall chart appearance.
        // ================================================================================

        // Create an XYChart object of size 640 x 350 pixels
        XYChart c = new XYChart(width, height);

        // Set the plotarea at (55, 50) with width 80 pixels less than chart
        // width, and height 85 pixels
        // less than chart height. Use a vertical gradient from light blue
        // (f0f6ff) to sky blue (a0c0ff)
        // as background. Set border to transparent and grid lines to white
        // (ffffff).
        c.setPlotArea(30, 0, c.getWidth() - 35, c.getHeight() - 30);

        // As the data can lie outside the plotarea in a zoomed chart, we need
        // enable clipping.
        c.setClipping();

        LegendBox b = c.addLegend(55, 25, false, "Arial Bold", 10);
        b.setBackground(Chart.Transparent);
        b.setLineStyleKey();

        // ================================================================================
        // Add data to chart
        // ================================================================================

        //
        // In this example, we represent the data by lines. You may modify the
        // code below to use other
        // representations (areas, scatter plot, etc).
        //

        // Add a line layer for the lines, using a line width of 2 pixels
        LineLayer layer = c.addLineLayer2();
        layer.setLineWidth(2);
        layer.setFastLineMode();

        // Now we add the 3 data series to a line layer, using the color red
        // (ff0000), green (00cc00)
        // and blue (0000ff)
        layer.setXData(viewPortTimeStamps);
        layer.addDataSet(viewPortDataSeriesA, 0xff0000);
        layer.addDataSet(viewPortDataSeriesB, 0x00cc00);
        layer.addDataSet(viewPortDataSeriesC, 0x0000ff);

        // ================================================================================
        // Configure axis scale and labelling
        // ================================================================================

        if (currentIndex > 0)
            c.xAxis().setDateScale(viewPortStartDate, viewPortEndDate);

        // For the automatic axis labels, set the minimum spacing to 75/30
        // pixels for the x/y axis.
        c.xAxis().setTickDensity(10);
        c.yAxis().setTickDensity(10);

        //
        // In a zoomable chart, the time range can be from a few years to a few
        // seconds. We can need
        // to define the date/time format the various cases.
        //

        // If all ticks are year aligned, we use "yyyy" as the label format.
        c.xAxis().setFormatCondition("align", 360 * 86400);
        c.xAxis().setLabelFormat("{value|yyyy}");

        // If all ticks are month aligned, we use "mmm yyyy" in bold font as the
        // first label of a year,
        // and "mmm" for other labels.
        c.xAxis().setFormatCondition("align", 30 * 86400);
        c.xAxis().setMultiFormat(Chart.StartOfYearFilter(), "<*font=bold*>{value|mmm yyyy}", Chart.AllPassFilter(), "{value|mmm}");

        // If all ticks are day algined, we use "mmm dd<*br*>yyyy" in bold font
        // as the first label of a
        // year, and "mmm dd" in bold font as the first label of a month, and
        // "dd" for other labels.
        c.xAxis().setFormatCondition("align", 86400);
        c.xAxis().setMultiFormat(Chart.StartOfYearFilter(), "<*block,halign=left*><*font=bold*>{value|mmm dd<*br*>yyyy}", Chart.StartOfMonthFilter(),
                "<*font=bold*>{value|mmm dd}");
        c.xAxis().setMultiFormat2(Chart.AllPassFilter(), "{value|dd}");

        // If all ticks are hour algined, we use "hh:nn<*br*>mmm dd" in bold
        // font as the first label of
        // the Day, and "hh:nn" for other labels.
        c.xAxis().setFormatCondition("align", 3600);
        c.xAxis().setMultiFormat(Chart.StartOfDayFilter(), "<*font=bold*>{value|hh:nn<*br*>mmm dd}", Chart.AllPassFilter(), "{value|hh:nn}");

        // If all ticks are minute algined, then we use "hh:nn" as the label
        // format.
        c.xAxis().setFormatCondition("align", 60);
        c.xAxis().setLabelFormat("{value|hh:nn}");

        // If all other cases, we use "hh:nn:ss" as the label format.
        c.xAxis().setFormatCondition("else");
        c.xAxis().setLabelFormat("{value|nn:ss:fff}");

        // We make sure the tick increment must be at least 1 second.
        c.xAxis().setMinTickInc(0.001);

        // ================================================================================
        // Output the chart
        // ================================================================================

        // We need to update the track line too. If the mouse is moving on the
        // chart (eg. if
        // the user drags the mouse on the chart to scroll it), the track line
        // will be updated
        // in the MouseMovePlotArea event. Otherwise, we need to update the
        // track line here.
        if (!chartViewer1.isInMouseMoveEvent()) {
            trackLineLabel(c, (null == viewer.getChart()) ? c.getPlotArea().getRightX() : viewer.getPlotAreaMouseX());
        }

        // Set the chart image to the ChartViewer
        chartViewer1.setChart(c);
    }

    //
    // Click event for the pointerPB.
    //
    private void pointerPB_Clicked() {
        chartViewer1.setMouseUsage(Chart.MouseUsageScrollOnDrag);
    }

    //
    // Click event for the zoomInPB.
    //
    private void zoomInPB_Clicked() {
        chartViewer1.setMouseUsage(Chart.MouseUsageZoomIn);
    }

    //
    // Click event for the zoomOutPB.
    //
    private void zoomOutPB_Clicked() {
        chartViewer1.setMouseUsage(Chart.MouseUsageZoomOut);
    }

    private static class SimpleExtensionFilter extends FileFilter {
        public String ext;

        public SimpleExtensionFilter(String extension) {
            this.ext = "." + extension;
        }

        public String getDescription() {
            return ext.substring(1);
        }

        public boolean accept(java.io.File file) {
            return file.isDirectory() || file.getName().endsWith(ext);
        }
    }

    private void chartViewer1_MouseMovedPlotArea(MouseEvent e) {
        ChartViewer viewer = (ChartViewer) e.getSource();
        trackLineLabel((XYChart) viewer.getChart(), viewer.getPlotAreaMouseX());
        viewer.updateDisplay();
    }

    //
    // Draw track line with data labels
    //
    private void trackLineLabel(XYChart c, int mouseX) {
        // Clear the current dynamic layer and get the DrawArea object to draw
        // on it.
        DrawArea d = c.initDynamicLayer();

        // The plot area object
        PlotArea plotArea = c.getPlotArea();

        // Get the data x-value that is nearest to the mouse, and find its pixel
        // coordinate.
        double xValue = c.getNearestXValue(mouseX);
        int xCoor = c.getXCoor(xValue);
        if (xCoor < plotArea.getLeftX())
            return;

        // Draw a vertical track line at the x-position
        d.vline(plotArea.getTopY(), plotArea.getBottomY(), xCoor, 0x888888);

        // Draw a label on the x-axis to show the track line position.
        String xlabel = "<*font,bgColor=123456*> " + c.xAxis().getFormattedLabel(xValue, "nn:ss:fff") + " <*/font*>";
        TTFText t = d.text(xlabel, "Arial Bold", 7);

        // Restrict the x-pixel position of the label to make sure it stays
        // inside the chart image.
        int xLabelPos = Math.max(0, Math.min(xCoor - t.getWidth() / 2, c.getWidth() - t.getWidth()));
        t.draw(xLabelPos, plotArea.getBottomY() + 6, 0xffffff);

        // Iterate through all layers to draw the data labels
        for (int i = 0; i < c.getLayerCount(); ++i) {
            Layer layer = c.getLayerByZ(i);

            // The data array index of the x-value
            int xIndex = layer.getXIndexOf(xValue);

            // Iterate through all the data sets in the layer
            for (int j = 0; j < layer.getDataSetCount(); ++j) {
                ChartDirector.DataSet dataSet = layer.getDataSetByZ(j);

                // Get the color and position of the data label
                int color = dataSet.getDataColor();
                int yCoor = c.getYCoor(dataSet.getPosition(xIndex), dataSet.getUseYAxis());
                String name = dataSet.getDataName();

                // Draw a track dot with a label next to it for visible data
                // points in the plot area
                if ((yCoor >= plotArea.getTopY()) && (yCoor <= plotArea.getBottomY()) && (color != Chart.Transparent) && (null != name)
                        && (0 < name.length())) {
                    d.circle(xCoor, yCoor, 4, 4, color, color);

                    String label = "<*font,bgColor=" + Integer.toHexString(color) + "*> " + c.formatValue(dataSet.getValue(xIndex), "{value|P4}")
                            + " <*/font*>";
                    t = d.text(label, "Arial Bold", 7);

                    // Draw the label on the right side of the dot if the mouse
                    // is on the left side the
                    // chart, and vice versa. This ensures the label will not go
                    // outside the chart image.
                    if (xCoor <= (plotArea.getLeftX() + plotArea.getRightX()) / 2)
                        t.draw(xCoor + 5, yCoor, 0xffffff, Chart.Left);
                    else
                        t.draw(xCoor - 5, yCoor, 0xffffff, Chart.Right);
                }
            }
        }
    }

}
